<?php
/**
 * @file
 * Integration with Link field.
 */

/**
 * Alters the link field form.
 */
function _acton_extlink_link_field_edit_form_alter(&$form, $form_state) {
  if ($form['#field']['type'] == 'link_field') {
    // Build option.
    $instance_settings = $form['#instance']['settings'];
    $form['instance']['settings']['acton_extlink'] = array(
      '#type' => 'checkbox',
      '#title' => t('External link'),
      '#default_value' => !empty($instance_settings['acton_extlink']),
      '#description' => t('When output, this link will be output as an external link if the URL is outside of this site.'),
    );
    $form['instance']['settings']['#element_validate'][] = '_acton_extlink_link_form_remove_empty_option';
  }
}

/**
 * Removes empty option value.
 */
function _acton_extlink_link_form_remove_empty_option($element, &$form_state) {
  $form_value = drupal_array_get_nested_value($form_state['values'], $element['#array_parents']);
  if (isset($form_value['acton_extlink']) && empty($form_value['acton_extlink'])) {
    unset($form_value['acton_extlink']);
    drupal_array_set_nested_value($form_state['values'], $element['#array_parents'], $form_value);
  }
}

/**
 * Implements hook_preprocess_link_formatter_link_default().
 */
function acton_extlink_preprocess_link_formatter_link_default(&$variables) {
  if (!empty($variables['field']['settings']['acton_extlink'])) {
    acton_extlink_preprocess_link_element($variables['element']);
  }
}

/**
 * Implements hook_preprocess_link_formatter_link_url().
 */
function acton_extlink_preprocess_link_formatter_link_url(&$variables) {
  if (!empty($variables['field']['settings']['acton_extlink'])) {
    acton_extlink_preprocess_link_element($variables['element']);
  }
}

/**
 * Implements hook_preprocess_link_formatter_link_domain().
 */
function acton_extlink_preprocess_link_formatter_link_domain(&$variables) {
  if (!empty($variables['field']['settings']['acton_extlink'])) {
    acton_extlink_preprocess_link_element($variables['element']);
  }
}

/**
 * Implements hook_preprocess_link_formatter_link_short().
 */
function acton_extlink_preprocess_link_formatter_link_short(&$variables) {
  if (!empty($variables['field']['settings']['acton_extlink'])) {
    acton_extlink_preprocess_link_element($variables['element']);
  }
}

/**
 * Implements hook_preprocess_link_formatter_link_label().
 */
function acton_extlink_preprocess_link_formatter_link_label(&$variables) {
  if (!empty($variables['field']['settings']['acton_extlink'])) {
    acton_extlink_preprocess_link_element($variables['element']);
  }
}

/**
 * Processes an external link element.
 */
function acton_extlink_preprocess_link_element(&$element) {
  if (!empty($element['url']) && acton_extlink_is_external($element['url'])) {
    $element['attributes']['class'] = !empty($element['attributes']['class']) ? ($element['attributes']['class'] . ' ' . ACTON_EXTLINK_CLASS) : ACTON_EXTLINK_CLASS;
  }
}
